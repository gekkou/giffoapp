import React from 'react'
import ReactDOM from 'react-dom/client'
import './styles.css';
import { GiffoApp } from './GiffoApp';

ReactDOM.createRoot(document.getElementById('root')).render(
  <React.StrictMode>
    <GiffoApp />
  </React.StrictMode>
)
