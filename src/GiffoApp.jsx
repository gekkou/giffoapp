import React, { useState } from 'react'
import { AddCategory, GifGrid } from './components';

export const GiffoApp = () => {
  const [categories, setCategories] = useState(['one punch man'])

  const onAddCategory = (newCategory) => {
    if (categories.includes(newCategory)) return;

    setCategories([
      newCategory,
      ...categories
    ])
  }

  return (
    <>
      <h1>GiffoApp</h1>
      <AddCategory onNewCategory={(e) => onAddCategory(e)} />
      {categories.map((category) => (
        <GifGrid
          key={category}
          category={category}
        />
      ))}
    </>
  )
}

